<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\http\Request;

class Comment extends Model
{
    public function createBook(Request $request){
        $this->user = $request->user;
        $this->book = $request->book;
        $this->text = $request->text;
        $this->photo = $request->photo;
        $this->avaliation = $request->avaliation;
        $this->save();
    }

    public function updateBook(Request $request, $id)
    {
        if($request->user){
            $this->user = $request->user;
        }
        if($request->book){
            $this->book = $request->book;
        }
        if($request->text){
            $this->text = $request->text;
        }
        if($request->photo){
            $this->photo = $request->photo;
        }
        if($request->avaliation){
            $this->avaliation = $request->avaliation;
        }
        $this->save();
    }
}
